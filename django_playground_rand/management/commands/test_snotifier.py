from django.core.management.base import BaseCommand

from django_simple_notifier.django_simple_notifier.models import (
    SnotifierEmailContactZammad,
)
from django_simple_notifier.django_simple_notifier.plugin_zammad import send


class Command(BaseCommand):
    def handle(self, *args, **options):
        # test recipients
        rep1 = SnotifierEmailContactZammad(
            email="kevin.olbrich@dolphin-it.de",
        )

        rep2 = SnotifierEmailContactZammad(
            email="kolbrich@dolphin-it.de",
        )

        # test message
        message = send(
            notifier_list=[rep1, rep2],
            subject="Class Test",
            text="Class Test",
        )
