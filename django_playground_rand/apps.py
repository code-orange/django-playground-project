from django.apps import AppConfig


class DjangoPlaygroundRandConfig(AppConfig):
    name = "django_playground_rand"
