from braces.forms import UserKwargModelFormMixin
from django import forms

from django_mdat_customer.django_mdat_customer.models import *


class DomainRegisterForm(UserKwargModelFormMixin, forms.Form):
    domain = forms.CharField(label="Domain", max_length=100)
    customer = forms.ModelChoiceField(queryset=MdatCustomers.objects.none())

    def __init__(self, *args, **kwargs):
        super(DomainRegisterForm, self).__init__(*args, **kwargs)
        self.fields["customer"].queryset = MdatCustomers.objects.filter(
            mdatmultitenancyusers__user=self.user
        )
