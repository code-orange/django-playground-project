from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from .forms import *


@login_required
def domain_register(request):
    template = loader.get_template("django_playground_forms/domain_register.html")

    template_opts = dict()

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = DomainRegisterForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # Playground: skip data processing
            return HttpResponseRedirect(
                "/whstack/domain/edit/" + request.POST["domain"]
            )

    # if a GET (or any other method) we'll create a blank form
    else:
        form = DomainRegisterForm(user=request.user)

    template_opts["form"] = form

    return HttpResponse(template.render(template_opts, request))
