from django.apps import AppConfig


class DjangoPlaygroundFormsConfig(AppConfig):
    name = "django_playground_forms"
